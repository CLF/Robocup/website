---
permalink: /teaching/
layout: single
title:  "Courses"
header:
  overlay_image: /assets/images/home_banner.jpg
classes: wide
categories: 
  - info
tags:
  - RoboCup
  - teaching
sidebar:
  - 
    image: /assets/images/teaching/laptop_kai.jpg
    image_alt: "Robocup at Home logo"
  - title: "Courses"
    text: |
      - [RoboCup@Home (PJ / S+Ü)](#wise)
---

## RoboCup@Home (Pj | S & Ü) (WiSe)
{: #wise }

For the introduction to the robot platform, exercises, tutorials and a seminar part for individual aspects of the robot platform are carried out at the beginning of the semester.

At the end of the semester, we will hold a mini-challenge, where a robot behaviour is to be developed in teams of 2-4 students.

This challenge serves as practical preparation for our participation in the [2025 RoboCup German Open](https://robocup.de/german-open/) and possibly in [2025 RoboCup Brasil](https://athome.robocup.org/).

Students enrolling in the 10LP Project will participate in the Tutorials (as needed) and focus on specific robot capabilities. They too will prepare for the German Open in March 11-16, 2025

### Modules
- Projekt (392175): 5 LP
  - 39-Inf-EGMI (Bachelor): vertiefendes Projekt
  - 39-M-Inf-GP Grundlagenprojekt (Master ISY): Projekt
- Seminar + Übung (392176+392177): 5 LP
  - 39-Inf-WP-R-x Robotik (Bachelor from 2023): S+Ü (graded)
  - 39-M-Inf-ASE-bas/app/app-foc (Master IISY from 2023): S+Ü (app graded)
  - 39-M-Inf-INT-adv-foc: S+Ü (graded)
- Projekt (392210): 10 LP (39-M-Inf-P) (ungraded)

### Contents

You will acquire a wide range of practical knowledge and experience in different areas such as object recognition, manipulation and robot-human interaction.

- Introduction in:
  - ROS: Basics, Simulation, Navigation, Manipulation
  - Robot Behavior Programming
  - Speech, Person and Object Recognition
- Challenge implementation
- Robocup@Home task implementation

### Requirements for participation

- no hard requirements
- Prior knowledge of mobile robotics, manipulation, computer vision, pattern recognition is welcomed.