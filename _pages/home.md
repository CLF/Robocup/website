---
layout: splash
permalink: /
hidden: true
header:
  overlay_color: "#5e616c"
  #overlay_filter: rgba(255, 0, 0, 0.5)
  #image: /assets/images/tiago/tiago_h.jpg
  #overlay_image: /assets/images/placeholder/random-robot-banner.jpg
  overlay_image: /assets/images/home_banner.jpg
  actions:
    - label: "<i class='fas fa-calendar'></i> Participate"
      url: "https://ekvv.uni-bielefeld.de/kvv_publ/publ/vd?id=477144295"
excerpt: >
  Each year we form a team of students <br>
  to participate in <a href="https://athome.robocup.org/">RoboCup@Home</a>
feature_row:
  - image_path: /assets/images/competition_th.jpg
    alt: "competition"
    title: "Competition"
    excerpt: "Everything about RoboCup@Home."
    url: "/competition/"
    btn_class: "btn--primary"
    btn_label: "Learn more"
  - image_path: /assets/images/teaching_th.jpg
    alt: "teaching"
    title: "Teaching"
    excerpt: "Courses provided by ToBi/CLF."
    url: "/teaching/"
    btn_class: "btn--primary"
    btn_label: "Learn more"
  - image_path: /assets/images/systems_th.jpg
    alt: "something"
    title: "Systems"
    excerpt: "Platforms and Systems used by ToBi"
    url: "/systems/"
    btn_class: "btn--primary"
    btn_label: "Learn more"  
pagination: 
  enabled: true
    
---

{% include feature_row %}


<h3 id="recent" class="archive__subtitle">{{ site.data.ui-text[site.locale].recent_posts | default: "Recent Posts" }}</h3>

{% if paginator %}
  {% assign posts = paginator.posts %}
{% else %}
  {% assign posts = site.posts %}
{% endif %}

{% assign entries_layout = page.entries_layout | default: 'list' %}
<div class="entries-{{ entries_layout }}">
  {% for post in posts %}
    {% include archive-single.html type=entries_layout %}
  {% endfor %}
</div>

{% include paginator.html %}
