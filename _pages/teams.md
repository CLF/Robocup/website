---
permalink: /teams/
layout: single
title: "Hall of Fame"
toc: true 
toc_label: Teams
toc_sticky: true
---

## ToBi 2023

![image-center]({{ site.url }}{{ site.baseurl }}/assets/images/teams/2023.jpg){: .align-center}

- Team Leader: Sven Wachsmuth, Leroy Rügemer
- Students: Linus Schwarz, Jonas Vaquet, Laurin Gräsner, Jonas Zilke, Dennis Holzmann, Michel Wagemann, Leander von Seelstrang, Carolin Klute, Katrin Lammerts, Zoe Klinger
- Associates: Johannes Kummert, David Leins, Bernd Heihoff
- Competitions: 
    - RoboCup World Cup 2023 in Bordeaux, Frankreich (@Home, 5th Place)
- Technical Committee: Johannes Kummert

## ToBi 2019

![image-center]({{ site.url }}{{ site.baseurl }}/assets/images/teams/2019.jpg){: .align-center}

- Team Leader: Sven Wachsmuth, Luca Lach, David Leins
- Students: Jan Pohlmeyer, Julia Niermann, Thorben Markmann, Markus Vieth
- Associates: Sebastian Meyer Zu Borgsen, Leroy Rügemer
- Competitions: 
    - RoboCup German Open 2019 in Magdeburg (@Home 1st Place)
- Technical Committee: Johannes Kummert

## ToBi 2018

![image-center]({{ site.url }}{{ site.baseurl }}/assets/images/teams/2018.jpg){: .align-center}

- Team Leader: Sven Wachsmuth, Florian Lier, Johannes Kummert
- Team Members: Felix Friese, Kai Konen, Sarah Schröder, Robert Feldhans, David Leins, Jan Patrick Nülle, Janneke Simmering, Philipp von Neumann-Cosel
- Associates: Leroy Rügemer, Sebastian Meyer zu Borgsen, Guillaume Walck, Patrick Renner, Dominik Sixt, Luca Lach 
- Competitions: 
    - RoboCup German Open 2018 in Magdeburg (@Home 4th Place)
    - Robocup World Cup 2018 SSPL in Montreal, Canada (@Home SSPL 1st Place)

## ToBi 2017

![image-center]({{ site.url }}{{ site.baseurl }}/assets/images/teams/2017.jpg){: .align-center}

- Team Leader: Sven Wachsmuth, Florian Lier, Sebastian Meyer zu Borgsen
- Team Coordinators: Luca Michael Lach, Johannes Kummert, Dominik Sixt
- Team Members: Sebastian Müller, Thilo Reinold, Bjarte Feldmann, Felix Friese, Kai Konen, Sarah Schröder, Lukas Hindemith, Robert Feldhans
- Associates: Guillaume Walck, Phillip Lücking, Nils Neumann, Leroy Rügemer
- Competitions: 
    - RoboCup German Open 2017 in Magdeburg (@Home 3rd Place)
    - Robocup World Cup 2017 in Nagoya, Japan (@Home OPL 3rd Place)

## ToBi 2016

![image-center]({{ site.url }}{{ site.baseurl }}/assets/images/teams/2016.jpg){: .align-center}

- Team Leader: Sven Wachsmuth, Sebastian Meyer zu Borgsen, Florian Lier, Timo Korthals
- Team Members: Marvin Barther, Julian Exner, Jonas Gerlach, Philip Kenneweg, Luca Michael Lach, Henri Neumann, Nils Neumann, Johannes Kummert, Leroy Rügemer, Tobias Schumacher, Suchit Sharma, Dominik Sixt
- Associates: Guillaume Walck, Phillip Lücking, Corinna Wewer
- Competitions: 
    - RoboCup European Open 2016 in Eindhoven (3rd Place)
    - Robocup World Cup 2016 in Leipzig (1st Place)

## ToBi 2015

![image-center]({{ site.url }}{{ site.baseurl }}/assets/images/teams/2015.jpg){: .align-center}

- Team Leader: Sven Wachsmuth, Sebastian Meyer zu Borgsen, Timo Korthals, Leon Ziegler 
- Team Members:  Leroy Rügemer, Günes Minareci, Christian Limberg, Jonas Gerlach, Kevin Gardeja, Svenja Kösters 
- Competitions: 
    - Robocup German Open 2015, Magdeburg, Germany (3rd Place)
    - Robocup World Cup 2015, Hefei, China (3rd Place)

## ToBi 2014

![image-center]({{ site.url }}{{ site.baseurl }}/assets/images/teams/2014.jpg){: .align-center}

- Team Leader: Sven Wachsmuth, Sebastian Meyer zu Borgsen, Timo Korthals, Leon Ziegler
- Team Members: Günes Minareci, Christian Klarhorst, Martin Holland, Andreas Langfeld, Leroy Rügemer, Christian Witte, Niksa Rasic, Suchit Sharmam, Kai Harmening
- Competitions: 
    - Robocup German Open 2014, Magdeburg, Germany (3rd Place)
    - Robocup World Cup 2014, Joao Pessoa, Brazil (4th Place)

## ToBi 2013

![image-center]({{ site.url }}{{ site.baseurl }}/assets/images/teams/2013.jpg){: .align-center}

- Team Leader: Sven Wachsmuth, Matthias Schöpfer, Leon Ziegler
- Team Members: Lukas Kettenbach, Sebastian Meyer zu Borgsen, Günes Minareci, Christian Ascheberg, Daniel Nacke, Jan Pöppel, Adriana-Victoria Dreyer, Ingo Killmann
- Competitions: 
    - Robocup World Cup 2013, Eindhoven, Netherlands (6th Place)

## ToBi 2012

![image-center]({{ site.url }}{{ site.baseurl }}/assets/images/teams/2012.jpg){: .align-center}

- Team Leader: Sven Wachsmuth, Matthias Schöpfer, Frederic Siepmann, Leon Ziegler, Florian Lier
- Team Members: Dennis Wigand, Hendrik ter Horst, Lukas Kettenbach, Michael Zeunert, Patrick Renner, Philipp Dresselhaus, Sebastian Meyer zu Borgsen, Soufian Jebbara, Viktor Losing
- Competitions: 
    - Robocup World Cup 2012, Mexico City, Mexico (3rd Place)

## ToBi 2011

![image-center]({{ site.url }}{{ site.baseurl }}/assets/images/teams/2011.jpg){: .align-center}

- Team Leader: Sven Wachsmuth, Frederic Siepmann, Leon Ziegler, Florian Lier, Denis Schulze
- Team Members: Alexander Baumann, Birte Carlmeyer, Marcel Dulisch, Lukas Kettenbach, Maikel Linke, Phillip Lücking, Marian Pohling, Viktor Richter, Tobias Röhlig, Norman Köster
- Competitions: 
    - Robocup World Cup 2011, Istanbul, Turkey (5th Place)

## ToBi 2010

![image-center]({{ site.url }}{{ site.baseurl }}/assets/images/teams/2010.jpg){: .align-center}

- Team Leader: Sven Wachsmuth, Frederic Siepmann, Denis Schulze, Marco Kortkamp
- Team Members: Leon Ziegler, Torben Töniges, Jan-Frederic Steinke, Lukas Twardon, Jonathan Helbach, Andreas Kipp, Matthias Schröder, Annika Rothert
- Associated Members: Agnes Swadzba, Florian Lier, Christian Lang, Raphael Golombek
- Competitions: 
    - Robocup World Cup 2010, Singapore (7th Place)

## ToBi 2009

![image-center]({{ site.url }}{{ site.baseurl }}/assets/images/teams/2009.jpg){: .align-center}

- Team Leader: Sven Wachsmuth, Marc Hanheide, Frederic Siepmann, Fang Yuan, Raphael Golombek, Thorsten Spexard, Roland Philippsen*
- Team Member: David Klotz, Johannes Wienke, Jens Otto, Leon Ziegler, Torben Töniges, Sebastian Schneider, Norman Köster
 (Robotics and Artificial Intelligence Lab, Stanford University)
- Competitions: 
    - Robocup World Cup 2009, Graz, Austria (8th Place)
