---
permalink: /competition/
layout: single
title:  "Competition"
classes: wide
header:
  header: "unsplash-gallery-image-2-th.jpg"
  overlay_image: /assets/images/competition/header.jpg
categories: 
  - RoboCup
  - RoboCup@Home
tags:
  - RoboCup 
  - RoboCup@Home
sidebar:
  - 
    image: /assets/images/athome_logo.jpg
    image_alt: "Robocup at Home logo"
  - title: "Media"
    text: |
      - [Videos](#videos)
      - [Gallery](#gallery)
gallery_one:
  - url: /assets/images/competition/istanbul2011.jpg
    image_path: /assets/images/competition/istanbul2011_th.jpg
    title: "RoboCup Istanbul 2011 "
  - url: /assets/images/competition/hefei2015.jpg
    image_path: /assets/images/competition/hefei2015_th.jpg
    title: "RoboCup Hefei 2015 "
  - url: /assets/images/competition/nagoya_all_teams.jpg
    image_path: /assets/images/competition/nagoya_all_teams_th.jpg
    title: "RoboCup Nagoya 2017"
gallery_robots:
  - url: /assets/images/competition/robots_bender.jpg
    image_path: /assets/images/competition/robots_bender_th.jpg
    title: "Bender"
  - url: /assets/images/competition/robots_cosero.jpg
    image_path: /assets/images/competition/robots_cosero_th.jpg
    title: "Cosero"
  - url: /assets/images/competition/robots_skuba.jpg
    image_path: /assets/images/competition/robots_skuba_th.jpg
    title: "Skuba"
gallery_two:
  - url: /assets/images/competition/rc_poster.jpg
    image_path: /assets/images/competition/rc_poster_th.jpg
    title: ""
  - url: /assets/images/competition/rc1.jpg
    image_path: /assets/images/competition/rc1_th.jpg
    title: ""
  - url: /assets/images/competition/rc2.jpg
    image_path: /assets/images/competition/rc2_th.jpg
    title: ""
  - url: /assets/images/competition/rc3.jpg
    image_path: /assets/images/competition/rc3_th.jpg
    title: ""
  - url: /assets/images/competition/rc4.jpg
    image_path: /assets/images/competition/rc4_th.jpg
    title: ""
  - url: /assets/images/competition/rc5.jpg
    image_path: /assets/images/competition/rc5_th.jpg
    title: ""
  - url: /assets/images/competition/rc_lan.jpg
    image_path: /assets/images/competition/rc_lan_th.jpg
    title: ""
---

## RoboCup@Home

The competition of RoboCup@Home features three leagues and consists of a series of tests which the robots have to solve. The Domestic Standard Platform League focuses on household tasks while the Social Standard Platform League has special interest in the social aspects of human robot interaction, both removing the hardware constraint. In contrast, the The Open Platform League has no hardware restrictions whatsoever and offers the highest degree of freedom when conducting research. The conducted tests will change over the years to become more advanced and function as an overall quality measurement in desired areas. Suggestions for tests can be discussed on the email list. Performance measurement is based on a score derived from competition rules and evaluation by a jury.

Criteria of the tests are listed below. The tests should:

- include human machine interaction
- be socially relevant
- be application directed/oriented
- be scientifically challenging
- be easy to set up and low in costs
- be simple and have self-explaining rules
- be interesting to watch
- take a small amount of time

### The Scenario

The ultimate scenario is the real world itself. To build up the required technologies gradually a basic home environment is provided as a general scenario. In the first years (the league started in 2006) it will consist of a living room and a kitchen but soon it should also involve other areas of daily life, such as a garden/park area, a shop, a street or other public places.

### Who can participate?

Everybody with an autonomous robot can participate. The @Home league consists of some tests and an open challenge to demonstrate the abilities of your robot. To participate in the open challenge you have to participate in at least one test. The competition is in a real world scenario.

{% include gallery caption="" id="gallery_robots"%}

## Media

### Videos

#### Best of Robocup 2015

<video width="100%" controls>
    <source src="{{ site.baseurl }}/assets/videos/bestof2015.webm" type="video/webm">
    Your browser does not support the video tag.
</video> 

#### Video of World Cup Final 2016

{% include video id="W2TQ1ua76HU" provider="youtube" %}

### Gallery

{% include gallery caption="" id="gallery_one"%}

{% include gallery caption="" id="gallery_two"%}
