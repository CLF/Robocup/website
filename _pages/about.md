---
permalink: /about/
title: "About Team of Bielefeld"
excerpt: "Team of Bielefeld (ToBi) for RoboCup@Home Open Platform League"
header:
  overlay_image: /assets/images/home_banner.jpg
#last_modified_at: 2022-05-27T11:59:26-04:00
toc: true
---

# What is ToBi

Team of Bielefeld (ToBi) is a team of students, PhD’s and employees of the University of Bielefeld that focuses on the development of robotics.

Each year new students participate in the RoboCup@Home courses and are engaged in the development of different service robot platforms. These students form the Team of Bielefeld (ToBi) to participate in the RoboCup@Home Open Platform League (OPL).

Beginning with our first participation in 2009 at the Robocup World Cup 2009 in Graz, Austria, our team has been able to qualify for the RoboCup World Championship each year. Since 2012, ToBi repeatedly ranked among the best teams in the @Home league and won multiple competitions.


# What is RoboCup?

RoboCup is an international scientific initiative with the goal to advance the state of the art of intelligent robots. When established in 1997, the original mission was to field a team of robots capable of winning against the human soccer World Cup champions by 2050.
