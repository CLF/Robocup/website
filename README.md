# Robocup ToBi Website

Build with [Jekyll]() and [minimal-mistakes]()

## Running page locally

To work locally with this project, you'll have to follow the steps below:

1. Fork, clone or download this project
1. Install bundler and git lfs `apt install ruby-bundler git-lfs`
1. Checkout lfs files `git lfs install && git lfs checkout`
1. Download dependencies: `bundle install`
1. Build and preview: `bundle exec jekyll serve`
1. Add content

The above commands should be executed from the root directory of this project.

Read more at Jekyll's [documentation][].

[Jekyll]: http://jekyllrb.com/
[documentation]: https://jekyllrb.com/docs/home/
[minimal-mistakes]: (https://mmistakes.github.io/minimal-mistakes/docs/quick-start-guide/)