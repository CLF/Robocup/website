---
title: "Biron"
excerpt: "**RETIRED** Legacy Platform"
header:
  overlay_color: "#333"
  #image: /assets/images/biron/biron_h.jpg
  teaser: /assets/images/biron/biron_th.jpg
date: 2010-01-01 12:00:00 -0000
sidebar:
  - 
    image: /assets/images/biron/biron.jpg
    #image_alt: "tiago waving"
  - title: "Specs"
    text: |
      - Height:	140cm
      - Weight: ?
      - Footprint: ?
gallery:
  - url: /assets/images/biron/1.jpg
    image_path: /assets/images/biron/1_th.jpg
    #alt: "placeholder image 1"
    title: "Biron during a Competition"
  - url: /assets/images/biron/2.jpg
    image_path: /assets/images/biron/2_th.jpg
    #alt: "placeholder image 2"
    title: "Closeup of two Brion Robots"
  - url: /assets/images/biron/biron_sensors.jpg
    image_path: /assets/images/biron/biron_sensors_th.jpg
    #alt: "placeholder image 3"
    title: "Birons sensor head"

---

The BIRON platform is based on the research platform GuiaBot by MobileRobots customized and equipped with sensors that allow analysis of the current situation. It comprises two piggyback laptops to provide the computational power and to achieve a system running autonomously and in real-time for HRI.ToBi platformThe robot base is a PatrolBot which is 59cm in length, 48cm in width, weighs approx. 45 kilograms with batteries. It is maneuverable with 1.7 meters per second maximum translation and 300+ degrees rotation per second. The drive is a two-wheel differential drive with two passive rear casters for balance. Inside the base there is a 180 degree laser range finder with a scanning height of ~30cm above the floor (SICK LMS, see image on the left). In contrast to most other PatrolBot bases, ToBi does not use an additional internal computer. The piggyback laptops are Core2Duo processors with 2GB main memory and are running Ubuntu Linux. The cameras that are used for person and object detection/recognition are 2MP CCD firewire cameras (Point Grey Grashopper, see iamge on the left). One is facing down for object detection/recognition, the second camera is facing up for face detection/recognition. For room classification and 3D object positions ToBi is equipped with an optical imaging system for real time 3D image data acquisition. 
Additionally the robot is equipped with a Katana IPR 5 degrees-of-freedom (DOF) arm (see image,  second from bottom on the right); a small and lightweight manipulator driven by 6 DC-Motors with integrated digital position encoders. The end-effector is a sensor-gripper with distance and touch sensors (6 inside, 4 outside) allowing to grasp and manipulate objects up to 400 grams throughout the arm's envelope of operation. The upper part of the robot houses a touch screen (~15in) as well as the system speaker. The on board microphone has a hyper-cardioid polar pattern and is mounted on top of the upper part of the robot. The overall height is approximately 140cm. 

{% include gallery caption="" %}
