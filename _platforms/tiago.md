---
title: "TIAGo"
excerpt: "Our current OPL Robot"
date: 2017-01-01 12:00:00 -0000
header:
  #overlay_color: "#333"
  image: /assets/images/tiago/tiago_h.jpg
  teaser: /assets/images/tiago/tiago_th.jpg
sidebar:
  - 
    image: /assets/images/tiago/wave.gif
    image_alt: "tiago waving"
  - title: "Specs"
    text: |
      - Height:	110-145 cm
      - Weight: 70 kg
      - Footprint: ø 54 cm
      - Arm Payload: 3 Kg (without end-effector)
      - Battery autonomy: 8-10h
gallery:
  - url: /assets/images/tiago/jaii_bielefeld_paderborn-1489.jpg
    image_path: /assets/images/tiago/jaii_bielefeld_paderborn-1489_th.jpg
    #alt: "placeholder image 1"
    title: "Image 1 title caption"
  - url: /assets/images/tiago/jaii_bielefeld_paderborn-1554.jpg
    image_path: /assets/images/tiago/jaii_bielefeld_paderborn-1554_th.jpg
    #alt: "placeholder image 2"
    title: "Image 2 title caption"
  - url: /assets/images/placeholder/unsplash-gallery-image-3.jpg
    image_path: /assets/images/placeholder/unsplash-gallery-image-3-th.jpg
    #alt: "placeholder image 3"
    title: "Image 3 title caption"

---

The robot platform TIAGo by PAL-Robotics is used in RoboCup@Home related research scenarios since 2019. It is equipped with laser sensors and sonars that allow nagivation and mapping, a pan-tilt head including rgb and depth cameras, and a compliant 7 DoF arm with a force/torque sensor and a parallel gripper. The robot arm is equipped with a SCHUNK gripper (model: WSG 025) with a maximum opening width of 68mm,  which has been customized in order to enhance its capability to grasp larger objects. We augmented the robot with an exchangable custom expansion. This turns the parallel motion into a scissoring motion to increase the maximum opening width. The gripper is designed to mount up to four adaptive fingers. We equipped the robot with a Lenovo P52 notebook running Ubuntu Linux 18.04 on a 3D-printed mount. Additional computing power is provided by a Jetson TX1 module and a Intel Neural Compute Stick 2. For improved speech recognition we added a Sennheiser MKE 400 shotgun microphone to the head.

{% include gallery caption="" %}
