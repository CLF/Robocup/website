---
title: "Pepper"
excerpt: "**RETIRED** SSPL Platform"
header:
  overlay_color: "#333"
  #image: /assets/images/biron/biron_h.jpg
  teaser: /assets/images/pepper/pepper_th.jpg
date: 2016-01-01 12:00:00 -0000
sidebar:
  - 
    image: /assets/images/pepper/pepper_head_s.jpg
    #image_alt: "tiago waving"
  - title: "Specs"
    text: |
      - Height: 120 cm
      - Weight: 28 kg
      - Footprint: 48.5 cm

---

Pepper competes in the Social Standard Plateform League.

{% include video id="70YMCihD1ds" provider="youtube" %}


