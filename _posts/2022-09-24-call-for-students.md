---
title:  "Call for Students!"
date:   2022-09-24 13:00:00 -0000
categories: 
    - robocup 
    - cfs
    - teaching
author: Leroy Rügemer
---

Each year we form a team of students to participate at the RoboCup@Home.

Details for participation: [ekvv](https://ekvv.uni-bielefeld.de/kvv_publ/publ/vd?id=71309989)

The Team of Bielefeld (ToBI) has regularly participated there between 2009 and 2019.

Here is a Video of our last Homecoming Event:

<video width="100%" controls>
  <source src="{{ site.baseurl }}/assets/videos/hc2019.webm" type="video/webm">
  Your browser does not support the video tag.
</video> 


