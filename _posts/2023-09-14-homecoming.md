---
title:  "Homecoming 2023"
date:   2023-09-15 17:00:00 -0000
    
categories: 
    - robocup 
    - cfs
    - teaching
author: Leroy Rügemer
---

Video of our Homeconing Event after Robocup2023. 

<video width="100%" preload controls>
  <source src="{{ site.baseurl }}/assets/videos/homecoming2023.webm" type="video/webm">
  Your browser does not support the video tag.
</video> 

Try Firefox if the Video is not playing

Download:

- [vp9]({{ site.baseurl }}/assets/videos/homecoming2023.webm).
