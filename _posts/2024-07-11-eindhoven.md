---
title:  "RoboCup Eindhoven 2024"
date:   2024-07-11 13:00:00 -0000
categories: 
    - robocup 
    - cfs
    - teaching
author: Kathrin Lammers

gallery_one:
  - url: /assets/images/RoboCup-2024/P1288255-3.JPG
    image_path: /assets/images/RoboCup-2024/P1288255-3.JPG
    title: "RoboCup Eindhoven 2024 "
  - url: /assets/images/RoboCup-2024/IMG_0977.jpg
    image_path: /assets/images/RoboCup-2024/IMG_0977.jpg
    title: ""
  - url: /assets/images/RoboCup-2024/P1288011m.jpg
    image_path: /assets/images/RoboCup-2024/P1288011m.jpg
    title: ""
  - url: /assets/images/RoboCup-2024/P1288077n.JPG
    image_path: /assets/images/RoboCup-2024/P1288077n.JPG
  - url: /assets/images/RoboCup-2024/P1288100.JPG
    image_path: /assets/images/RoboCup-2024/P1288100.JPG
    title: ""
gallery_lab:
  - url: /assets/images/RoboCup-2024-Lab/20240703_112003.jpg
    image_path: /assets/images/RoboCup-2024-Lab/20240703_112003.jpg
    title: ""
  - url: /assets/images/RoboCup-2024-Lab/20240703_112006.jpg
    image_path: /assets/images/RoboCup-2024-Lab/20240703_112006.jpg
    title: ""
  - url: /assets/images/RoboCup-2024-Lab/20240703_125408.jpg
    image_path: /assets/images/RoboCup-2024-Lab/20240703_125408.jpg
    title: ""
  - url: /assets/images/RoboCup-2024-Lab/20240703_125526.jpg
    image_path: /assets/images/RoboCup-2024-Lab/20240703_125526.jpg
    title: ""
  - url: /assets/images/RoboCup-2024-Lab/20240703_125554.jpg
    image_path: /assets/images/RoboCup-2024-Lab/20240703_125554.jpg
    title: ""
---

In einer Zeit, in der KI Bilder erschaffen und Texte schreiben kann, kann man sich gerne mal die
Frage stellen: 
***Wie lange noch, bis KI mir nicht nur dabei helfen kann, eine Hausarbeit zu schreiben, sondern auch meinen Abwasch macht oder mir die Einkäufe einräumt?***

Solche Aufgaben, die in der physischen und nicht der digitalen Welt erledigt werden müssen, werden seit 2006 jedes Jahr bei der RoboCup@Home Weltmeisterschaft gestellt. Teams von wissenschaftlichen Einrichtungen aus aller Welt kommen mit ihren Robotern und stellen sich der Herausforderung.

Auch in diesem Jahr nimmt das **Team of Bielefeld (ToBi)** wieder an diesem Wettbewerb teil – mit unserem Roboter TIAGo von PAL Robotics treten wir vom **17. bis 21. Juli 2024 in Eindhoven** gegen 16 andere Teams an – und hoffen auf den Sieg. Unsere Mannschaft besteht – neben dem Roboter – aus Studierenden, Tutoren, Doktoranden und Dozenten der Technischen Fakultät der Uni Bielefeld, die sich im letzten Winter vorbereitet und seit Beginn des Jahres fleißig an der Umsetzung von 9 verschiedenen Aufgaben für den RoboCup gearbeitet haben. Unter der Leitung von Sven Wachsmuth und Leroy Rügemer haben Michal, Mohamed, Alex, Niklas und Zoe gemeinsam mit ihren Tutoren Michel und Laurin dafür gesorgt, dass unser Roboter inzwischen einiges kann. Einkäufe einräumen, Gepäck tragen, Müsli servieren, das Geschirr in die Spülmaschine stellen – diese und andere Aufgaben werden nächste Woche beim RoboCup präsentiert. Des Weiteren unterstützen die wissenschaftlichen Mitarbeiter David Leins und Dennis Holzmann dort das Team.

Nach dem **zweiten Platz bei den German Open** im April diesen Jahres, hoffen wir darauf, dass es auch beim internationalen Wettbewerb für eine Platzierung auf dem Treppchen reicht. Die Vorbereitungen laufen bereits auf Hochtouren. Am Sonntag wird dann der Roboter eingepackt und auf geht es in die Niederlande!

### Gallery

{% include gallery caption="" id="gallery_one"%}

{% include gallery caption="" id="gallery_lab"%}