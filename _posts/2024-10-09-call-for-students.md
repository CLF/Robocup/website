---
title:  "Call for Students 2024!"
date:   2024-09-10 13:00:00 -0000
categories: 
    - robocup 
    - cfs
    - teaching
author: Dennis Holzmann
---

Each year we form a team of students to participate at the RoboCup@Home.

Details for participation: 

[Project 5LP: RoboCup@Home](https://ekvv.uni-bielefeld.de/kvv_publ/publ/vd?id=477143119)

[Project 10 LP: RoboCup@Home](https://ekvv.uni-bielefeld.de/kvv_publ/publ/vd?id=486210639)

[Tutorium](https://ekvv.uni-bielefeld.de/kvv_publ/publ/vd?id=477144295) & [Seminar: RoboCup@Home](https://ekvv.uni-bielefeld.de/kvv_publ/publ/vd?id=477143243)

The Team of Bielefeld (ToBI) has regularly participated there between 2009 and 2023.


