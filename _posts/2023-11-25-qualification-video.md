---
permalink: /q2024
title:  "Qualification 2024"
date:   2023-11-25 13:00:00 -0000
categories: 
    - robocup 
    - qualification
    - video
author: Leroy Rügemer
---

This is our Qualification Video for the 2024 Robocup Eindhoven. 
You can Download the Team Description Paper [here]({{ site.baseurl }}/assets/etc/TDP_2024.pdf)

<video width="100%" controls>
  <source src="{{ site.baseurl }}/assets/videos/qualification2024.webm" type="video/webm">
  Your browser does not support the video tag.
</video> 

Download:

- [vp9]({{ site.baseurl }}/assets/videos/qualification2024.webm).



