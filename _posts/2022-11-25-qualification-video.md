---
permalink: /q2023
title:  "Qualification 2023"
date:   2022-11-25 13:00:00 -0000
categories: 
    - robocup 
    - qualification
    - video
author: Leroy Rügemer
---

This is our Qualification Video for Robocup2023. 
You can Download the Team Description Paper [here]({{ site.baseurl }}/assets/etc/TDP_2023.pdf)

<video width="100%" controls>
  <source src="{{ site.baseurl }}/assets/videos/qualification2023.webm" type="video/webm">
  Your browser does not support the video tag.
</video> 

Download in 1080p:

- [h265]({{ site.baseurl }}/assets/videos/qualification2023.mp4).
- [vp9]({{ site.baseurl }}/assets/videos/qualification2023-hd.webm).



